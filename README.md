## Project Setup

```sh
npm install
```

### Compile and Hot-Reload for Development

```sh
npm run dev
```

### Run Unit Tests with [Vitest](https://vitest.dev/)

```sh
npm run test:unit
```

### Lint with [ESLint](https://eslint.org/)

```sh
npm run lint 
```

## Info
Na projektu jsem strávil cca. 6 hodin, využil jsem příležitost a vyzkoušel Vue 3 a composition api, se kterými jsem zatím neměl tolik zkušeností, takže mi to trvalo o něco déle, než by tomu bylo u 2.6 / 2.7.
