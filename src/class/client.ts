import Storage from '@/class/storage'

import type { Word } from '@/types.ts'

interface APIClientParams {
  url: string
}

export default class APIClient extends Storage {
  rawUrl: APIClientParams['url'] = ''

  constructor({ url }: { url: APIClientParams['url'] }) {
    super({ name: url })

    this.rawUrl = url
  }

  async getResponse(): Promise<any> {
    if (this.getStorage.length) {
      return this.getStorage
    }

    try {
      const response = await fetch(this.rawUrl)
      const json = (await response.json()) as Array<Word>

      this.setStorage = { data: json }

      return json
    } catch (error) {
      window.alert(`Api error - ${error}`)
    }
  }
}
