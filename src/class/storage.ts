interface APIClientParams {
  name: string
  data: Array<any>
}

export default class Storage {
  rawName: APIClientParams['name'] = ''

  constructor({ name }: { name: APIClientParams['name'] }) {
    this.rawName = name
  }

  get getStorage(): APIClientParams["data"]{
    const localData = localStorage.getItem(this.rawName)

    if (localData) {
      return JSON.parse(localData)
    }

    return []
  }

  set setStorage({ data }: { data: APIClientParams['data'] }) {
    localStorage.setItem(this.rawName, JSON.stringify(data))
  }
}
