export interface Word {
  name: string
  uuid: string
}

export interface WordList extends Word {
  random: number
}
