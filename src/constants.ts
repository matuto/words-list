export const API = Object.freeze({
  WORDS_1000: 'https://run.mocky.io/v3/9cea67b1-6665-4df4-90d4-0acc9f75ed38',
  WORDS_10000: 'https://run.mocky.io/v3/10b3d19f-2f34-4318-94a4-3633d4b26c8c'
})
