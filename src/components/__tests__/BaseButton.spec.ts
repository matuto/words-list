import { describe, it, expect } from 'vitest'

import { mount } from '@vue/test-utils'
import BaseButton from '@/components/BaseButton.vue'

describe('BaseButton', () => {
  it('Renders correct default slot', () => {
    const defaultSlot = 'faker'
    const wrapper = mount(BaseButton, {
      slots: { default: defaultSlot }
    })

    expect(wrapper.text()).toContain(defaultSlot)
  })

  it('Renders correct theme class', () => {
    const wrapper = mount(BaseButton, {
      props: { theme: 'danger' }
    })

    expect(wrapper.classes()).toEqual(['base-button', '--danger'])
  })
})
