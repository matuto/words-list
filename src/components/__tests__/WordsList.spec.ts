import { describe, it, expect, vi } from 'vitest'

import { mount } from '@vue/test-utils'

import { faker } from '@faker-js/faker'

import type { Word } from '@/types'

import List from '@/components/WordsList.vue'

global.fetch = vi.fn()

function createFetchResponse(data) {
  return { json: () => new Promise((resolve) => resolve(data)) }
}

const setListData = (): Array<Word> => {
  const data = []

  for (let i = 0; i <= 20; ++i) {
    data.push({
      name: faker.lorem.word(),
      uuid: faker.datatype.uuid()
    })
  }

  return data
}

const listData = setListData()

vi.stubGlobal('fetch', async () => {
  return {
    json() {
      return listData
    }
  }
})

describe('List', () => {
  it('Fetch data ', async (): Promise<void> => {
    const wrapper = mount(List)

    await (wrapper.vm as any).getData()

    expect((wrapper.vm as any).list).toEqual(listData)
  })

  it('Add new Item ', (): void => {
    const wrapper = mount(List)

    const item = listData[0]

    ;(wrapper.vm as any).onAdd(item)

    expect((wrapper.vm as any).list[0]).toEqual(item)
  })

  it('Delete item ', (): void => {
    const wrapper = mount(List)

    ;(wrapper.vm as any).list = [...listData]
    ;(wrapper.vm as any).onDelete(0)

    expect((wrapper.vm as any).list[0]).toEqual(listData[1])
  })

  it('Edit item ', (): void => {
    const wrapper = mount(List)

    const newVal = faker.lorem.word()
    const index = 0

    ;(wrapper.vm as any).list = [...listData]
    ;(wrapper.vm as any).onUpdate(newVal, index)

    expect((wrapper.vm as any).list[index].name).toEqual(newVal)
  })
})
